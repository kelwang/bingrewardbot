import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Dict {
	
	private ArrayList<String> words;
	
	
	public Dict(String workingDir) throws IOException {
		this.words = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(new File(
				workingDir+"dictionary.txt")));
		String inputLine = null;
		
		while ((inputLine = reader.readLine()) != null) {
			// Split the input line.
			this.words.add(inputLine);
		}
	}
	
	public String getRandomWord(){
		if(this.words.size() == 0){
			return "";
		}else{
			int length = this.words.size();
			Random rand = new Random();
			int n = rand.nextInt(length);
			return this.words.get(n);
		}
	}
}
