import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

	/**
	 * @param args
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public static void main(String[] args) throws InterruptedException,
			IOException, URISyntaxException {

		String workingDir = "";
		if (args.length > 0) {
			workingDir += args[0];
		}
		// The Firefox driver supports javascript

		PrintWriter writer = new PrintWriter(workingDir + "output.txt", "UTF-8");
		Dict dict = new Dict(workingDir);
		Account account = new Account(workingDir);
		ArrayList<String[]> accounts = account.getAccounts();
		ExecutorService executor = Executors.newFixedThreadPool(5);
		for (String[] i : accounts) {
			executor.submit(new Processor(dict, writer, i[0], i[1], i[2]));

		}
		executor.shutdown();
		executor.awaitTermination(1, TimeUnit.DAYS);
		writer.close();

	}

}