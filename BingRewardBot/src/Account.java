import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Account {
	private ArrayList<String[]> accounts;
	
	public Account(String workingDir) throws IOException{
		this.accounts = new ArrayList<String[]>();
		BufferedReader reader = new BufferedReader(new FileReader(new File(
				workingDir+"accounts.txt")));
		String inputLine = null;
		
		while ((inputLine = reader.readLine()) != null) {
			// Split the input line.
			this.accounts.add(inputLine.split("\\s"));
		}
	}
	
	public ArrayList<String[]> getAccounts(){
		return this.accounts;
	}
}
