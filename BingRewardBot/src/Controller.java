import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Controller {
	/**
	 * 
	 * @param dict
	 * @param username
	 * @param password
	 * @param account_type
	 * @throws InterruptedException
	 */
	public static void runIt(Dict dict, PrintWriter writer, String username,
			String password, String account_type) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"/home/kwang/chromedriver");
		ChromeOptions option = new ChromeOptions();
		WebDriver driver = new ChromeDriver(option);

		// Go to the Google Suggest home page

		login(driver, username, password, account_type);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("http://www.bing.com");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("http://www.bing.com/rewards/dashboard");

		// pc search
		HashMap<String, Integer> allowed = getMaxAllowed(driver);
		
		String winHandleBefore = driver.getWindowHandle();
		additionalSearch(driver);
		driver.switchTo().window(winHandleBefore);
		
		writer.println("working on: " + username + "," + password + ","
				+ account_type + "\n" + allowed.toString());
		search(driver, dict, allowed.get("pc").intValue());
		driver.quit();

		if (allowed.get("mobile").intValue() > 0) {
			// mobile search
			option = setMobile(option);
			driver = new ChromeDriver(option);
			login(driver, username, password, account_type);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.get("http://www.bing.com");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.get("http://www.bing.com/rewards/dashboard");
			search(driver, dict, allowed.get("mobile").intValue());
			driver.quit();
		}

	}

	public static void additionalSearch(WebDriver driver)
			throws InterruptedException {
		String[] patterns = { "http://www\\.bing\\.com/rewards/share([^\\s]*)",
				"http://www\\.bing\\.com/explore/rewards-mobile([^\\s]*)",
				"http://www\\.bing\\.com/explore/rewards-searchearn([^\\s]*)" };
		List<WebElement> li_list = driver.findElement(
				By.id("dashboard_wrapper")).findElements(By.tagName("li"));
		for (WebElement e : li_list) {
			e = e.findElement(By.tagName("a"));
			String progress = e.findElement(By.className("progress")).getText();
			String href = e.getAttribute("href");
			Boolean clickable = true;
			clickable = Pattern.compile("([\\d]+)\\sof\\s([\\d]+)\\scredit")
					.matcher(progress).find();

			for (String pattern : patterns) {
				if (href == null || href.matches(pattern)) {
					clickable = false;
				}
			}
			if (clickable) {
				e.click();
			}
		}
	}

	public static ChromeOptions setMobile(ChromeOptions options) {
		options.addArguments("--user-agent=Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16");
		return options;
	}

	/**
	 * 
	 * @param driver
	 * @return {HashMap<String, Integer>}
	 */
	public static HashMap<String, Integer> getMaxAllowed(WebDriver driver) {
		HashMap<String, Integer> result = new HashMap<String, Integer>();

		String pattern = "([\\d]+)\\sof\\s([\\d]+)\\scredits";
		String Mobile_arg = driver.findElement(
				By.xpath("//*[@id='mobsrch01']/div[2]")).getText();
		Integer mobile_result = new Integer(0);
		String PC_arg = driver
				.findElement(
						By.xpath("//a[@class='tile rel blk tile-height' and contains(., 'PC search')]/div[2]"))
				.getText();
		Integer pc_result = new Integer(0);

		String current_score_text = driver.findElement(
				By.xpath("//div[@class=\"credits\"]")).getText();

		Matcher mobile_matcher = Pattern.compile(pattern).matcher(Mobile_arg);
		if (mobile_matcher.find()) {
			mobile_result = new Integer(mobile_matcher.group(2));
		}

		Matcher pc_matcher = Pattern.compile(pattern).matcher(PC_arg);
		if (pc_matcher.find()) {
			pc_result = new Integer(pc_matcher.group(2));
		}

		result.put("mobile", mobile_result);
		result.put("pc", pc_result);
		result.put("result", new Integer(current_score_text));

		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param username
	 * @param password
	 * @throws InterruptedException
	 */
	public static void login(WebDriver driver, String username,
			String password, String account_type) throws InterruptedException {
		if (account_type.equals("hotmail")) {
			driver.get("https://login.live.com");
			driver.findElement(By.name("login")).sendKeys(username);
			driver.findElement(By.name("passwd")).sendKeys(password);
			driver.findElement(By.name("SI")).submit();
			Thread.sleep(5000);
		} else {
			driver.get("https://www.facebook.com/login.php?skip_api_login=1&api_key=111239619098&signed_next=1&next=https%3A%2F%2Fwww.facebook.com%2Fv1.0%2Fdialog%2Foauth%3Fredirect_uri%3Dhttps%253A%252F%252Fssl.bing.com%252Ffd%252Fauth%252Fsignin%253Faction%253Dfacebook_oauth%2526provider%253Dfacebook%26auth_type%3Dhttps%26client_id%3D111239619098%26ret%3Dlogin&cancel_uri=https%3A%2F%2Fssl.bing.com%2Ffd%2Fauth%2Fsignin%3Faction%3Dfacebook_oauth%26provider%3Dfacebook%26error%3Daccess_denied%26error_code%3D200%26error_description%3DPermissions%2Berror%26error_reason%3Duser_denied%23_%3D_&display=page");
			driver.findElement(By.name("email")).sendKeys(username);
			driver.findElement(By.name("pass")).sendKeys(password);
			driver.findElement(By.name("login")).submit();
			Thread.sleep(5000);
		}

	}

	/**
	 * 
	 * @param driver
	 * @param dict
	 * @param max
	 * @throws InterruptedException
	 */
	public static void search(WebDriver driver, Dict dict, int max)
			throws InterruptedException {
		for (int i = 0; i < max * 2; i++) {

			String url = "http://www.bing.com/search?q=" + dict.getRandomWord();
			// System.out.println(url);
			driver.get(url);
			Thread.sleep(1000);
		}
	}
}
