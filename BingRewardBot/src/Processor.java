import java.io.PrintWriter;

public class Processor implements Runnable {
	private Dict dict;
	private PrintWriter writer;
	private String username;
	private String password;
	private String account_type;

	public Processor(Dict dict, PrintWriter writer, String username,
			String password, String account_type) {
		this.dict = dict;
		this.writer = writer;
		this.username = username;
		this.password = password;
		this.account_type = account_type;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Controller.runIt(this.dict, this.writer, this.username,
					this.password, this.account_type);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}